﻿using System;
using Demos.Models;

namespace Demos.Reflection
{
    public class RuntimeTypeDemo
    {
        public static void Run()
        {
            Type typeofUser = typeof(User);

            int zero = 0;
            Type typeofZero = zero.GetType();

            Console.WriteLine($"typeofUser == typeofZero is {typeofUser == typeofZero}");
            Console.WriteLine($"typeofZero == typeof(int) is {typeofZero == typeof(int)}");
        }
    }
}