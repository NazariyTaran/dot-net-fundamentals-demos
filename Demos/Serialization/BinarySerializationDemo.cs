﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Demos.Models;

namespace Demos.Serialization
{
    public class BinarySerializationDemo
    {
        public const string FILE_PATH = @"E:\test-data\book-state.bin";

        public static void Run()
        {
            var objectState = new Book(456987)
            {
                Id = 1,
                Name = "The Time Machine",
                Author = new Author
                {
                    Id = 2,
                    Name = "Herbert Wells"
                }
            };

            var formatter = new BinaryFormatter();
            using (var fileStream = new FileStream(FILE_PATH, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fileStream, objectState);

                Console.WriteLine(fileStream.Length);
            }

            using (var fileStream = new FileStream(FILE_PATH, FileMode.Open))
            {
                // set breakpoint here to see deserialized obj in debug mode
                var obj = (Book)formatter.Deserialize(fileStream);
            }
        }
    }
}