﻿namespace Demos.Models.Enums
{
    public enum UserType
    {
        Active = 0,
        Suspended = 1,
        Deleted = 2
    }
}