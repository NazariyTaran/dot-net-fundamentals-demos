﻿using System;
using System.Linq;
using System.Reflection;
using SampleAssembly.Attributes;

namespace Demos.Reflection
{
    public class AttributesDemo
    {
        public static void Run()
        {
            Assembly assembly = Assembly.Load(AssemblyName.GetAssemblyName("SampleAssembly.dll"));

            Type typeofConsoleWrapper = assembly.GetType("SampleAssembly.ConsoleWrapper");

            object consoleWrapperInstance = Activator.CreateInstance(typeofConsoleWrapper);

            SerializableAttribute serializableAttribute = typeofConsoleWrapper.GetCustomAttribute<SerializableAttribute>();

            MethodInfo methodsWithMethodMarker = typeofConsoleWrapper.GetMethods()
                .First(method => method.GetCustomAttribute<MethodMarkerAttribute>() != null);

            ParameterInfo parameterInfo = methodsWithMethodMarker.GetParameters().First();
            var parameterMarker = parameterInfo.GetCustomAttribute<ParameterMarkerAttribute>();

            int parameter = 5 * parameterMarker.MultiplyOn;
            methodsWithMethodMarker.Invoke(consoleWrapperInstance, new object[] { parameter });
        }
    }
}