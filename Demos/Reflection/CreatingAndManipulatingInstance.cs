﻿using System;
using System.Reflection;

namespace Demos.Reflection
{
    public class CreatingAndManipulatingInstance
    {
        public static void Run()
        {
            Assembly assembly = Assembly.Load(AssemblyName.GetAssemblyName("SampleAssembly.dll"));

            Type typeofConsoleWrapper = assembly.GetType("SampleAssembly.ConsoleWrapper");

            object consoleWrapperInstance = Activator.CreateInstance(typeofConsoleWrapper);
            MethodInfo writeHelloWorldMethod = typeofConsoleWrapper.GetMethod("WriteHelloWorld");
            writeHelloWorldMethod.Invoke(consoleWrapperInstance, null);

            object consoleWrapperParametrizedInstance = Activator.CreateInstance(typeofConsoleWrapper, 33);
            typeofConsoleWrapper.InvokeMember("WriteNumber", BindingFlags.InvokeMethod, null, consoleWrapperParametrizedInstance, null);
        }
    }
}