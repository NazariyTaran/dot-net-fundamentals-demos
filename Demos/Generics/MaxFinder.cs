﻿using System;

namespace Demos.Generics
{
    public static class MaxFinder
    {
        public static TType FindMax<TType>(TType[] array) where TType : IComparable<TType>
        {
            if (array == null || array.Length == 0)
            {
                return default(TType);
            }

            var max = array[0];
            for (var i = 1; i < array.Length; i++)
            {
                if (max.CompareTo(array[i]) == -1)
                {
                    max = array[i];
                }
            }

            return max;
        }
    }
}