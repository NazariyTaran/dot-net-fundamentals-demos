﻿using System;
using System.IO;
using System.IO.Compression;

namespace Demos.Streams
{
    public class StreamDemo
    {
        public static void Run()
        {
            // TODO: you need to create file anywhere on your computer to make this demo work
            // write path to file you create here
            // in my case I had file with 3 following lines:
            //first line
            //second line
            //third line
            string pathToRead = @"E:\test-data\to-read.txt";

            using (FileStream stream = File.OpenRead(pathToRead))
            {
                while (stream.Position != stream.Length)
                {
                    var @byte = stream.ReadByte();
                    Console.Write((char)@byte);
                }

                stream.Position = stream.Length - 8;
                while (stream.Position != stream.Length)
                {
                    var @byte = stream.ReadByte();
                    Console.Write((char)@byte);
                }

                stream.Seek(-8, SeekOrigin.End);
                while (stream.Position != stream.Length)
                {
                    var @byte = stream.ReadByte();
                    Console.Write((char)@byte);
                }
            }

            Console.WriteLine();
            Console.WriteLine();

            using (FileStream stream = File.OpenRead(pathToRead))
            using (var streamReader = new StreamReader(stream))
            {
                while (!streamReader.EndOfStream)
                {
                    Console.WriteLine(streamReader.ReadLine());
                }
            }
        }
    }
}