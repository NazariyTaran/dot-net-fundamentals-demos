﻿using System;

namespace Demos.Extensions
{
    // extension method demo
    public static class StringExtensions
    {
        public static void Print(this string str, string str2)
        {
            Console.WriteLine($"First string is '{str}'");
            Console.WriteLine($"Second string is '{str2}'");
        }
    }
}