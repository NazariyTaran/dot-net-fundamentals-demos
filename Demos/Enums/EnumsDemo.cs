﻿using System;
using Demos.Models.Enums;

namespace Demos.Enums
{
    public class EnumsDemo
    {
        public static void Run()
        {
            UserRole userRole = UserRole.Regular;
            Console.WriteLine($"userRole: '{userRole}', '{userRole:D}'");

            userRole |= UserRole.Seller | UserRole.Buyer;
            Console.WriteLine($"userRole: '{userRole}', '{userRole:D}'");

            userRole &= UserRole.Seller | UserRole.Admin;
            Console.WriteLine($"userRole: '{userRole}', '{userRole:D}'");
        }
    }
}