﻿using System;
using System.Text.RegularExpressions;

namespace Demos.Regexp
{
    public static class RegexDemo
    {
        public static void Run()
        {
            string pattern = @"\$\d+(,\d{1,2})?";

            string[] inputs = { "$12", "$12,45", "12$" };
            foreach (string input in inputs)
            {
                Match match = Regex.Match(input, pattern);
                Console.WriteLine($"Input {input} {(match.Success ? "match" : "does not match")}");
            }
        }
    }
}