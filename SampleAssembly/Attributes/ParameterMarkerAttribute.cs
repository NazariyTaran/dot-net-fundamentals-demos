﻿using System;

namespace SampleAssembly.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class ParameterMarkerAttribute : Attribute
    {
        public int MultiplyOn { get; set; }
    }
}