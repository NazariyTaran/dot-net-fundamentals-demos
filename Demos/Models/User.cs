﻿using System;

namespace Demos.Models
{
    public class User
    {
        public event Action<string> NameChanged;
        // you can assign empty delegate to event to avoid null checking
        // public event Action<string> NameChanged = delegate { };

        private string name;

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                this.NameChanged?.Invoke(this.name);
            }
        }
    }
}