﻿using System.Collections.Generic;

namespace Demos.Delegates
{
    public delegate bool FilterPredicate(int number);

    public static class Filter
    {
        public static List<int> FilterInt(this IEnumerable<int> collection, FilterPredicate predicate)
        {
            var filtered = new List<int>();
            foreach (var item in collection)
            {
                if (predicate(item))
                {
                    filtered.Add(item);
                }
            }

            return filtered;
        }
    }
}