﻿using System;
using SampleAssembly.Attributes;

namespace SampleAssembly
{
    [Serializable]
    public class ConsoleWrapper
    {
        private readonly int number;

        public ConsoleWrapper()
        {
        }

        public ConsoleWrapper(int number)
        {
            this.number = number;
        }

        public void WriteHelloWorld()
        {
            Console.WriteLine("Hello world!");
        }

        public void WriteNumber()
        {
            Console.WriteLine($"Number is {this.number}");
        }

        [MethodMarker]
        public void MethodWithAttribute([ParameterMarker(MultiplyOn = 5)]int argument)
        {
            Console.WriteLine($"Passed argument is {argument}");
        }
    }
}