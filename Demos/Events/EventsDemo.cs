﻿using System;
using Demos.Models;

namespace Demos.Events
{
    public class EventsDemo
    {
        public static void Run()
        {
            var user = new User { Name = "Jack" };
            user.NameChanged += OnUserNameChange;

            user.Name = "Vasya";
            user.NameChanged -= OnUserNameChange;

            user.Name = "Petya";
        }

        private static void OnUserNameChange(string newName)
        {
            Console.WriteLine($"New name is {newName}");
        }
    }
}