﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Demos.Models;

namespace Demos.Serialization
{
    public class XmlSerializationDemo
    {
        public static void Run()
        {
            var objectState = new Book(456987)
            {
                Id = 1,
                Name = "The Time Machine",
                Author = new Author
                {
                    Id = 2,
                    Name = "Herbert Wells"
                }
            };

            var stringWriter = new StringWriter();
            var serializer = new XmlSerializer(objectState.GetType());
            serializer.Serialize(stringWriter, objectState);

            string xml = stringWriter.ToString();

            Console.WriteLine(xml);

            var xmlReader = XmlReader.Create(new StringReader(xml));
            var restoredObj = (Book)serializer.Deserialize(xmlReader);
        }
    }
}