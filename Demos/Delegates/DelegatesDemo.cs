﻿using System;

namespace Demos.Delegates
{
    public class DelegatesDemo
    {
        public static void Run()
        {
            var intCollection = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            Console.WriteLine(string.Join(",", intCollection.FilterInt(OnlyOddNumber)));
        }

        private static bool OnlyOddNumber(int value)
        {
            return (value & 1) != 0;
        }
    }
}