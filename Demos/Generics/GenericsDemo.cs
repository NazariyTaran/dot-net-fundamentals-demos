﻿using System;

namespace Demos.Generics
{
    public class GenericsDemo
    {
        public static void Run()
        {
            Console.WriteLine(MaxFinder.FindMax(new[] { 1, 2, 6, 8, 3, 4 }));
        }
    }
}