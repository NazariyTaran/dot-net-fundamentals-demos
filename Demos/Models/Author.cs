﻿using System;

namespace Demos.Models
{
    [Serializable]
    public class Author
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}