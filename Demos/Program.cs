﻿using Demos.Delegates;
using Demos.Enums;
using Demos.Events;
using Demos.Generics;
using Demos.Reflection;
using Demos.Regexp;
using Demos.Serialization;
using Demos.Streams;
using Demos.Strings;

namespace Demos
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            //EnumsDemo.Run();

            //GenericsDemo.Run();

            //DelegatesDemo.Run();

            //EventsDemo.Run();

            //StringParsingDemo.Run();

            //RegexDemo.Run();

            //RuntimeTypeDemo.Run();

            //CreatingAndManipulatingInstance.Run();

            //AttributesDemo.Run();

            //StreamDemo.Run();

            BinarySerializationDemo.Run();

            XmlSerializationDemo.Run();

            JsonSerializationDemo.Run();
        }
    }
}