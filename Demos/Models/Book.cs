﻿using System;

namespace Demos.Models
{
    [Serializable]
    public class Book
    {
        private int serialNumber;

        public long Id { get; set; }

        public string Name { get; set; }

        private int PublishYear { get; set; }

        public Author Author { get; set; }

        public Book()
        {
        }

        public Book(int serialNumber)
        {
            this.serialNumber = serialNumber;
            this.PublishYear = serialNumber;
        }
    }
}