﻿using System;

namespace SampleAssembly.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MethodMarkerAttribute : Attribute
    {
    }
}