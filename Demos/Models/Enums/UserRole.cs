﻿using System;

namespace Demos.Models.Enums
{
    [Flags]
    public enum UserRole
    {
        Regular = 1,
        Buyer = 2,
        Seller = 4,
        Admin = 8,
        SuperAdmin = 16
    }
}