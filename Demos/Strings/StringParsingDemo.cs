﻿using System;
using System.Globalization;

namespace Demos.Strings
{
    public static class StringParsingDemo
    {
        public static void Run()
        {
            string date = "20162010";
            Console.WriteLine(DateTime.ParseExact(date, "yyyyddMM", CultureInfo.InvariantCulture));
        }
    }
}