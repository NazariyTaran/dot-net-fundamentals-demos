﻿using System;
using Demos.Models;
using Newtonsoft.Json;

namespace Demos.Serialization
{
    public class JsonSerializationDemo
    {
        public static void Run()
        {
            var objectState = new Book(456987)
            {
                Id = 1,
                Name = "The Time Machine",
                Author = new Author
                {
                    Id = 2,
                    Name = "Herbert Wells"
                }
            };

            string json = JsonConvert.SerializeObject(objectState, Formatting.Indented);

            Console.WriteLine(json);

            var restoredObject = JsonConvert.DeserializeObject<Book>(json);
        }
    }
}